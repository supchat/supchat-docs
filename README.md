# SupChat integration

All communication with your users is managing via our mobile apps. They are available on Play Store and App Store.
To integrate chat with your project you need an API key. You ca receive the key when sign up using our landing page or in our applications.

**supchat.chat**:

<img src="img/web_email_input.jpg"
     height="120px"
     alt="Web"/>
     
**Mobile Application**:

  <div>
  <span>
  <img src="img/mobile_1.jpg"
     height="300px"
     alt="Web"/>
  </span>
    <span>
    <img src="img/mobile_2.jpg"
       height="300px"
       alt="Web"/>
    </span>
  </div>

## Flutter

Add SupChat dependency in `pubspec.yaml`:

```yaml
  supchat_client: ^1.1.1
```

Init SupChat, recommended in your `main()`:

```dart
  import 'package:supchat_client/client_entry_point.dart';
  ...
  initSupChat(LibraryConfig(customerId: API_KEY));
```

When you want to open a chat screen just run:
```dart
  import 'package:supchat_client/client_entry_point.dart';
  ...
  Navigator.of(context).push(MaterialPageRoute(builder: (context) => SupChatApp(customUserDetails: {"testKey": "textValue"})));
```
Where API_KEY is the key you got from previous step. As you can see SupChat here is represented by `SupChatApp` widget. You can use your own route transition or wrap this widget with your own widget. 

## Web

Just add this piece of code before closing `body tag`:

```javascript
    <script>
        //Set your APP_ID
        var APP_ID = YOUR_APP_ID;

        window.supchatSettings = {
            app_id: APP_ID
        };
        (function (d, h, w) {
            s = d.createElement('script'), s.src = "https://supchat.chat/widget/js/supchat.js", s.async = !0, e = d.getElementsByTagName(h)[0], e.appendChild(s), s.addEventListener('load', function (e) {
            }, !1)
        })(document, 'head', window);
    </script>
```
